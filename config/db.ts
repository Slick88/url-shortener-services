import mongoose from 'mongoose'
import config from 'config'

const db = config.get('mongoUri')

// @ts-ignore
const connectDB = async () => {
    try {
        // @ts-ignore
        await mongoose.connect(db, {
            useNewUrlParser: true
        });

        console.log('Mongoose DB connected successfully')
    } catch (e) {
        console.error(e.message)
        process.exit(1)
    }
}

export default connectDB;
