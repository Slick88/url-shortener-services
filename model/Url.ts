import mongoose, { Schema, Document } from 'mongoose'

const urlScheme = new Schema({
    code: String,
    longUrl: String,
    shortUrl: String,
    data: { type: String, default: Date.now()}
})

export interface IUrlScheme extends Document {
    code: String,
    longUrl: String,
    shortUrl: String,
    data: String
}

export default mongoose.model<IUrlScheme>('Url', urlScheme);

