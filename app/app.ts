// lib/app.ts
import express = require('express');
import bodyParser from 'body-parser';
import shortLinkRouter from "./shortLinkRouter";
import cors from 'cors';
import connectDB from "../config/db";
import Url from '../model/Url'
import index from "./index";

connectDB();
var PORT = process.env.PORT || 5000
// Create a new express application instance
const app: express.Application = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors())

// parse application/json
app.use(bodyParser.json());

app.use('/', index());

app.use('/shortlink', shortLinkRouter())

app.listen(PORT, function() {
  console.log('Example app listening on port 5000!');
});
