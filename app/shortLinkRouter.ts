import {Router} from 'express';
import moment from "moment";
import  validURl from 'valid-url';
import shortId from 'shortid';
import axios from 'axios';
import config from 'config';

import Url from '../model/Url'

const safeUrlCheck = (url: string) => {
    const googleAPI: string = config.get('googleAPI')
    const threatTypes = config.get('googleThreatTypes')
    const platformTypes = config.get('googlePlatformTypes')
    const threatEntryTypes = config.get('googleThreatEntryTypes')
    const body = {
        client: {
            clientId: "yourcompanyname",
            clientVersion: "1.5.2"
        },
        threatInfo: {
            threatTypes,
            platformTypes,
            threatEntryTypes,
            "threatEntries": [
                {
                    "url": url
                }
            ]
        }
    }

    return axios.post(googleAPI, body)
        .then(res => {
            return res.data.matches === undefined
        })
}

const putShortLink = async (req: any, res: any) => {
    console.log('putShortLink: ', req.body)
    const { longUrl } = req.body
    const baseUrl = config.get('baseUrl')

    const isSafe = await safeUrlCheck(longUrl)


    if(!validURl.isUri(<string>baseUrl) && isSafe){
        return res.status(401).json('Invalid base url')
    }

    const code = shortId.generate()

    if(validURl.isUri(<string>longUrl)){
        try {
            let url = await Url.findOne({longUrl})
            if(url){
                res.json(url)
            } else {
                const shortUrl = baseUrl + '/' + code;
                url = new Url({
                    longUrl,
                    shortUrl,
                    code,
                    date: new Date()
                })
                await url.save()

                res.json(url)
            }
        } catch (e) {
            console.log(e)
            res.status(500).json('Server Error')
        }
    } else {
        res.status(401).json('Invalid Long Url')
    }

}


export default function shortLinkRouter(): Router {
    const router = Router();
    router.use(function timeLog(req, res, next) {
        console.log('Entered shortLinkRouter: ', moment().format('MMMM Do YYYY, h:mm:ss a'));
        next();
    });
    router.post('/', (req, res) => putShortLink(req,res))
    return router;
};
