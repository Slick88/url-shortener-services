import {Router} from 'express';
import moment from "moment";

import Url from '../model/Url'

const getLongLink = async (req: any, res: any) => {
    console.log('get LongLink: ', req.params.code)
    try {
        const url = await Url.findOne({code: req.params.code})
        if(url) {
            res.redirect(url.longUrl)
        } else {
            res.status(404).json('No Url Found')
        }
    } catch (e) {
        res.status(503).json('Server Error')
    }
}

export default function indexRouter(): Router {
    const router = Router();
    router.use(function timeLog(req, res, next) {
        console.log('Entered index: ', moment().format('MMMM Do YYYY, h:mm:ss a'));
        next();
    });
    router.get('/:code', (req, res) => getLongLink(req,res))
    return router;
};
